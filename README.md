# KulturImmer Web App

<img src="./public/logo192text.png" alt="KulturImmer logo" />

A React application to display cultural events that are available online.

## Development

Make sure to have the latest LTS of NodeJS installed. Then create a `.env` file with the following content:

```ini
REACT_APP_API_URL=https://api.kulturimmer.de
```

Now, run the following commands and open your browser at [http://localhost:8080](http://localhost:8080):

```shell
$ npm ci
$ npm start
```

## Deployment

The application can easily be deployed via a Docker container. To build the container, run:

```shell
$ docker build -t kulturimmer-webapp:dev .
```

Afterwards the application can be run via:

```shell
$ docker run --rm -p 8080:80 kulturimmer-webapp:dev
```

## License

This project is licensed under the terms of the [MIT license](./LICENSE.md).
