const apiUrl = process.env.REACT_APP_API_URL;

const fetchApi = async (method, url, payload) => {
  try {
    const response = await fetch(`${apiUrl}${url}`, {
      method,
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify(payload)
    });

    const json = await response.json();

    return json.data || json.error;
  } catch (err) {
    console.error(err);
  }
};

export const findEvents = () => fetchApi('GET', '/events');
