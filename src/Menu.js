import React from 'react';
import { NavLink } from 'react-router-dom';
import './Menu.css';

const Menu = () => (
  <nav className='menu'>
    <img className='menu_logo' src='logo192.png' alt='Logo'></img>
    <h1 className='menu_title'>
      <span className='menu_title_initials'>K</span>ultur
      <span className='menu_title_initials'>I</span>mmer
    </h1>
    <NavLink
      exact
      to='/favorites'
      activeClassName='menu_button_active'
      className='menu_button menu_button_favorites'
    >
      Meine Events
    </NavLink>
    <NavLink
      exact
      to='/'
      activeClassName='menu_button_active'
      className='menu_button menu_button_events'
    >
      Alle Events
    </NavLink>
    <a
      href='https://devpost.com/software/kulturimmer'
      className='menu_button menu_button_about'
    >
      Über uns
    </a>
  </nav>
);

export default Menu;
