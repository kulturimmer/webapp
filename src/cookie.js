const getCookieData = cookieName => {
  const cookies = document.cookie.split(';');
  const foundCookie = cookies.find(cookie => {
    const [name] = cookie.split('=', 1);
    return name.trim() === cookieName;
  });
  if (foundCookie) {
    return decodeURI(foundCookie.split('=', 2).pop() || '');
  }
  return '';
};

const setCookieData = (cookieName, cookieData) => {
  document.cookie = `${cookieName}=${encodeURI(cookieData)}`;
};

export const set = (key, value) => {
  const cookieName = 'state';
  const cookieData = getCookieData(cookieName) || '{}';
  const state = JSON.parse(cookieData);
  state[key] = value;
  const encodedState = JSON.stringify(state);
  setCookieData(cookieName, encodedState);
};

export const get = key => {
  const cookieName = 'state';
  const cookieData = getCookieData(cookieName) || '{}';
  const state = JSON.parse(cookieData);
  return state[key];
};
