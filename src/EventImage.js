import React from 'react';
import PropTypes from 'prop-types';
import './EventImage.css';

const Placeholder = ({ imageUrl, title, category }) => {
  let placeholderSrc = '';
  switch (category) {
    case 'Literatur': {
      placeholderSrc = `placeholders/placeholder_literature.jpeg`;
      break;
    }
    case 'Sport': {
      placeholderSrc = `placeholders/placeholder_sport.jpeg`;
      break;
    }
    case 'Musik': {
      placeholderSrc = `placeholders/placeholder_music.jpeg`;
      break;
    }
    case 'Kreativität': {
      placeholderSrc = `placeholders/placeholder_creativity.jpeg`;
      break;
    }
    default: {
      placeholderSrc = `placeholders/placeholder_theatre.jpeg`;
      break;
    }
  }

  return imageUrl ? (
    <img src={imageUrl} alt={title} className='eventimage' />
  ) : (
    <img
      src={placeholderSrc}
      alt='Placeholder for event'
      className='eventimage'
    />
  );
};

Placeholder.propTypes = {
  title: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired
};

export default Placeholder;
