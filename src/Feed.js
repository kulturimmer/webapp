import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import Menu from './Menu';
import Polaroid from './Polaroid';
import { findEvents } from './client';
import { get, set } from './cookie';
import './Feed.css';

const Feed = () => {
  const { pathname } = useLocation();
  const [events, setEvents] = useState([]);
  const [likes, setLikes] = useState(get('likes') || {});
  const like = eventId => e => {
    e.preventDefault();
    e.stopPropagation();
    const newLikes = { ...likes, [eventId]: true };
    set('likes', newLikes);
    setLikes(newLikes);
  };
  const unlike = eventId => e => {
    e.preventDefault();
    e.stopPropagation();
    const newLikes = Object.keys(likes).reduce((total, key) => {
      if (key === eventId) return total;
      return { ...total, [key]: true };
    }, {});
    set('likes', newLikes);
    setLikes(newLikes);
  };
  useEffect(() => {
    const load = async () => {
      const events = await findEvents();
      if (pathname === '/favorites') {
        return setEvents(events.filter(event => !!likes[event.id]));
      }
      return setEvents(events);
    };
    load();
  }, [likes, pathname]);
  return (
    <div className='feed'>
      <Menu></Menu>
      <main className='feed_container'>
        {events.map(event => (
          <Polaroid
            liked={!!likes[event.id]}
            onLike={like(event.id)}
            onUnlike={unlike(event.id)}
            key={event.id}
            event={event}
          ></Polaroid>
        ))}
      </main>
    </div>
  );
};

export default Feed;
