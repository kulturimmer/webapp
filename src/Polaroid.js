import React from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as HeartFilled } from './vectors/heart_filled.svg';
import { ReactComponent as HeartOutlined } from './vectors/heart_outlined.svg';
import EventImage from './EventImage';
import './Polaroid.css';

const Polaroid = ({ event, liked, onLike, onUnlike }) => {
  return (
    <a
      href={event.url}
      target='_blank'
      rel='noopener noreferrer'
      className='polaroid'
    >
      {liked ? (
        <HeartFilled className='polaroid_like' onClick={onUnlike} />
      ) : (
        <HeartOutlined className='polaroid_like' onClick={onLike} />
      )}
      <div className='polaroid_image'>
        <EventImage
          imageUrl={event.image_url}
          title={event.title}
          category={event.category}
        />
      </div>
      <div className='polaroid_content'>
        <h2 className='polaroid_title'>{event.title}</h2>
      </div>
    </a>
  );
};

Polaroid.propTypes = {
  event: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image_url: PropTypes.string.isRequired,
    category: PropTypes.string.isRequired
  }).isRequired,
  liked: PropTypes.bool.isRequired,
  onLike: PropTypes.func.isRequired,
  onUnlike: PropTypes.func.isRequired
};

export default Polaroid;
