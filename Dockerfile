# Define base image for build process.
FROM node:12-buster AS build

# Configure working directory.
WORKDIR /app

# Copy dependency lists.
COPY package.json package-lock.json ./

# Install dependencies.
RUN npm ci

# Copy source files.
COPY . ./

# Define build time variables.
ARG REACT_APP_API_URL

# Build React application.
RUN npm run build

# Define runtime base image.
FROM nginx:mainline-alpine

# Copy optimized virtual host configuration.
COPY config/nginx.conf /etc/nginx/conf.d/default.conf

# Copy React app from previous stage.
COPY --from=build /app/build /usr/share/nginx/html

# Expose default NGinX port.
EXPOSE 80

# Start NGinX server.
CMD ["nginx", "-g", "daemon off;"]
